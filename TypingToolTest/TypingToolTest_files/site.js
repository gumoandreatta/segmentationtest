
/* Created 15/01/2017 by Fabian Salamo  @Yama Group */

//Define arrays for each segment:
//Arrays positions correspond to questions in this way: Q1, Q2, Q3_1, Q3_2, Q4


var segment1 = [0.530, -0.498, 0.301, 0.593, 0.352];

var segment2 = [-0.683, -0.263, 0.547, -0.076, 0.441];

var segment3 = [0.447, -0.639, -0.595, -0.463, -0.438];

var segment4 = [-0.294, 1.399, -0.253, -0.054, -0.356];

var segmentConstants = [-3.733, -0.357, 7.479, -3.389];

var currentQuestion = 1;
var partialAnswers = '';
var canContinue = false;
var savedID = 0;
var obj = {};
var saveSegment;
var launchedFromAccount = false;
var resultCode;
var answerSelection = '';
var globalSelected;
var values;
var segmentDoctor;
var lastQuestion = false;
var answ1, answ2, answ3, answ4, answ5;

//This method calculates the value for the segment depending on the submitted answers
var getSegment = function getSegment(num) {
    var sum = 0,
        answers = [answ1, answ2, answ3, answ4, answ5],
        segments = [segment1, segment2, segment3, segment4];

    for (var i = 0; i < answers.length; i++)
        sum += parseInt(answers[i]) * segments[num - 1][i];

    return Math.exp(sum + segmentConstants[num - 1]);
};

$(document).ready(function () {
    callBackAccountSelected("");
    $('.modal').modal();
});

window.addEventListener('load', function () {
    new FastClick(document.body);
}, false);


//Check if user launched media from an account record and returns false if he did not
function accountSelected() {
    com.veeva.clm.getDataForCurrentObject("Account", "Id", callBackAccountSelected);
}

function callBackAccountSelected(result) {
    if (result.code == 1112) {
        //No account was selected prior to segmentation
        $(".startButton").hide();
        setTimeout(function () { alert(items[54]); }, 40);
    } else {
        //Normaly start the application
        launchedFromAccount = true;
        $(".welcome").html(items[65]);
        $(".welcomeMessage").html(items[45]);
        $(".startButton").html(items[46]);
        $("#prevButton").text(items[48]);
        $("#nextButton").text(items[47]);
        $(".disclaimerTxt").text(items[60]);
    }
}

function startSegmentation() {
    if (launchedFromAccount == false) {
        //Close presentation
    } else {
        //Load the first question
        $(".disclaimerTxt").css("color", "#1c2d58");
        loadFirstQuestion();
        //hide home & show first question 
        $("#homeSlide").fadeOut(function () {
            $("#presentationSlide").fadeIn();
        })
    }
}

function loadFirstQuestion() {
    $(".titleQuestion").html(titleQuestion1);
    $(".question").html(question1);
    $(".answerContainer").html(answerSet1);
}

function next() {
    if (canContinue || $(".answerSelection").hasClass("selected") === true) {
        if (lastQuestion !== true) {
            animateNext();
            answerSelection += globalSelected;
            $("#prevButton").removeClass("disabled");
            saveAnswer();
            $("#fadeComponents").fadeOut(0, function () {
                $('ul').fadeOut();
                if ($("#nextButton").text() == items[49]) {
                    setTimeout(function () {
                        showSegmentation();
                    }, 3000);
                } else {
                    if (currentQuestion == 1) {
                        $(".titleQuestion").removeClass("ball01");
                        $(".titleQuestion").html(titleQuestion2);
                        $(".question").html(question2);
                        $(".answerContainer").html(answerSet2);
                        $(".titleQuestion").addClass("ball02");
                        if (answ2 != null) {
                            selectAnswer(answ2);
                        }
                    }
                    if (currentQuestion == 2) {
                        $(".titleQuestion").removeClass("ball02");
                        $(".ballProgress02").addClass("ballPast");
                        $(".titleQuestion").html(titleQuestion3);
                        $(".question").html(question3);
                        $(".answerContainer").html(answerSet3);
                        $(".titleQuestion").addClass("ball03");

                        if (answ3 != null) {
                            selectAnswer(answ3);
                        }
                    }
                    if (currentQuestion == 3) { 
                        $(".titleQuestion").removeClass("ball03");
                        $(".ballProgress03").addClass("ballPast");
                        $(".titleQuestion").html(titleQuestion4);
                        $(".question").html(question4);
                        $(".answerContainer").html(answerSet4);
                        $(".titleQuestion").addClass("ball04");

                        if (answ4 != null) {
                            selectAnswer(answ4);
                        }
                    }
                    if (currentQuestion == 4) {
                        $(".titleQuestion").removeClass("ball04");
                        $(".ballProgress04").addClass("ballPast");
                        $(".titleQuestion").html(titleQuestion5);
                        $(".subquestionContainer").hide();
                        $(".question").html(question5);
                        $(".answerContainer").html(answerSet5);
                        $(".titleQuestion").addClass("ball05");

                    }
                    if (currentQuestion + 1 === 5) {
                        lastQuestion = true;
                        $("#nextButton").text(items[49]);
                    }

                    $("#fadeComponents").fadeIn();
                    $('ul').fadeIn();
                    currentQuestion = currentQuestion + 1;
                }
            });
            canContinue = false;
        }
        else {
            //save last answer ang get segmentation result
            saveAnswer();
            showSegmentation()
            $('.resulting-segment-modal').text(saveSegment);

            $(".welcome").html("Thanks for using the</br> Typing Tool");
            $(".welcomeMessage").html("<a class=\"modal-trigger\" href=\"#modal1\">Resulting segment</a>");
            $(".welcomeMessage").addClass("resulting-segment");
            $(".buttonContainer").hide();
            $('.resulting-segment-modal').html(saveSegment);

            var image = saveSegment.split(' ')[1];
            $('.modal-content > img').attr('src', 'TypingToolTest_files/images/' + image + '.svg')
            $("#presentationSlide").fadeOut(function () {
                $('#modal1').modal('close');
                $("#homeSlide").fadeIn();

            })

        }
    } else {
        alert(items[50]);
    }
}

function animateNext() {
    $(".progress").animate({
        width: "+=23%",
    }, 500, function () {
    });
}

function animatePrev() {
    $(".progress").animate({
        width: "-=23%",
    }, 500, function () {
    });
}

function prev() {
    if (currentQuestion == 1) {
        return;
    }
    canContinue = false;
    //fade out container
    animatePrev();
    $('ul').fadeOut();
    $("#fadeComponents").fadeOut(0, function () {
        $("#nextButton").text(items[47]);
        $("#nextButton").show();
        //set new content
        if (currentQuestion == 2) {
            $(".ballProgress").addClass("ballPast");
            $("#prevButton").addClass("disabled");
            $(".answerContainer").removeClass("answerContainerZoom")
            $(".titleQuestion").html(titleQuestion1);
            $(".question").html(question1);
            $(".answerContainer").html(answerSet1);
            $(".titleQuestion").removeClass("ball02");
            $(".titleQuestion").addClass("ball01");


        }//Load question 2:
        if (currentQuestion == 3) {
            $(".question").html(question2);
            $(".subquestionContainer").hide();
            $(".answerContainer").html(answerSet2);
            $(".titleQuestion").html(titleQuestion2);
            $(".titleQuestion").removeClass("ball03");
            $(".titleQuestion").addClass("ball02");
        }
        if (currentQuestion == 4) { 
            $(".question").html(question3);
            $(".answerContainer").html(answerSet3);
            $(".titleQuestion").html(titleQuestion3);
            $(".titleQuestion").removeClass("ball04");
            $(".titleQuestion").addClass("ball03");
        }
        if (currentQuestion == 5) {
            $(".subquestionContainer").show();
            $(".question").html(question4);
            $(".answerContainer").html(answerSet4);
            $(".titleQuestion").html(titleQuestion4);
            $(".titleQuestion").removeClass("ball05");
            $(".titleQuestion").addClass("ball04");
            lastQuestion = false;
        }
        currentQuestion = currentQuestion - 1;
        if (currentQuestion == 1) {
            $("#prevButton").addClass("disabled");
        }
        //add selected class to selected previously selected value
        var prevAnsw = getPrevAnswer();
        selectAnswer(prevAnsw);

        var aux = answerSelection.substring(0, answerSelection.length - 1);
        answerSelection = aux;
        $("#fadeComponents").fadeIn('400');
        $('ul').fadeIn('600');
        var aux = partialAnswers.substring(0, partialAnswers.length - 2);
        partialAnswers = aux;
    });
}

function getPrevAnswer() {
    if (currentQuestion == 1) {
        return answ1;
    }
    if (currentQuestion == 2) {
        return answ2;
    }
    if (currentQuestion == 3) {
        return answ3;
    }
    if (currentQuestion == 4) {
        return answ4;
    }
    if (currentQuestion == 5) {
        return answ5;
    }
}


function select(selected) {
    canContinue = true;
    var aux = selected.split(',')[1];
    //Sets the background for the selected item
    selectAnswer(aux);
}

function saveAnswer() {
    var elem = $(".selected").attr("id")[15];

    if (currentQuestion == 1) {
        answ1 = elem;
    }
    if (currentQuestion == 2) {
        answ2 = elem;
    }
    if (currentQuestion == 3) {
        answ3 = elem;
    }
    if (currentQuestion == 4) {
        answ4 = elem;
    }
    if (currentQuestion == 5) {
        answ5 = elem;
    }

    if (currentQuestion == 1) {
        if (elem != 1) {
            elem = 0;
        }
    }
    if (partialAnswers.length < 1) {
        partialAnswers = $("#partialResult").val();
    }
    partialAnswers += elem + '-';

}

function saveID(result) {
    savedID = result.ID;
    alert(savedID);

}

function unSelect() {
    var partialAnswers = $("#partialResult").val();
    partialAnswers += selected + '-';
    console.log('Partial answers: ' + partialAnswers);
}

function selectAnswer(selected) {
    var answer = 'answerSelection' + selected;
    for (var i = 0; i < 8; i++) {
        $("#answerSelection" + i).removeClass("selected");
    }
    $("#" + answer).addClass("selected");
    canContinue = true;
    globalSelected = selected;
}

function testSegmentation(array) {
    partialAnswers = array;
    var s1 = getSegment(1);
    var s2 = getSegment(2);
    var s3 = getSegment(3);
    var s4 = getSegment(4);
    var total = s1 + s2 + s3 + s4;
    var p1 = Math.round((s1 * 100) / total);
    var p2 = Math.round((s2 * 100) / total);
    var p3 = Math.round((s3 * 100) / total);
    var p4 = Math.round((s4 * 100) / total);
    var values = 'Segmentation: ' + p1 + '% -' + p2 + '% -' + p3 + '% -' + p4 + '%';
    var max = 0;
    var segment = 0;
    var arr = [p1, p2, p3, p4];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
            segment = i + 1;
        }
    }
    if (segment == 1) {
        saveSegment = items[56];
    }
    if (segment == 2) {
        saveSegment = items[57];
    }
    if (segment == 3) {
        saveSegment = items[58];
    }
    if (segment == 4) {
        saveSegment = items[59];
    }
    console.log("values: " + values);
}

function showSegmentation() {
    $(".buttonsContainer").fadeOut();
    var s1 = getSegment(1);
    var s2 = getSegment(2);
    var s3 = getSegment(3);
    var s4 = getSegment(4);
    var total = s1 + s2 + s3 + s4;
    var p1 = Math.round((s1 * 100) / total);
    var p2 = Math.round((s2 * 100) / total);
    var p3 = Math.round((s3 * 100) / total);
    var p4 = Math.round((s4 * 100) / total);
    values = 'Segmentation: ' + p1 + '% -' + p2 + '% -' + p3 + '% -' + p4 + '%';
    var max = 0;
    var segment = 0;
    var arr = [p1, p2, p3, p4];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
            segment = i + 1;
        }
    }
    if (segment == 1) {
        segmentDoctor = items[56];
        saveSegment = items[56];
    }
    if (segment == 2) {
        segmentDoctor = items[57];
        saveSegment = items[57];
    }
    if (segment == 3) {
        segmentDoctor = items[58];
        saveSegment = items[58];
    }
    if (segment == 4) {
        segmentDoctor = items[59];
        saveSegment = items[59];
    }
    //com.veeva.clm.getDataForCurrentObject("Account", "Id", callBackSave);
    storeAnswer("");
   // console.log("alerta: " + storeAnswer);
}

function callBackSave(result) {
    obj.Account_vod__c = result.Account.Id;
    obj.ABV_IE_Qualitative_Segmentation__c = saveSegment;
    com.veeva.clm.createRecord("Product_Metrics_vod__c", obj, storeAnswer);
}

function storeAnswer(result) {
    $("#thankYou").text(items[51]);
    $("#segmentTitle").text(items[52]);
    $("#segment").text(saveSegment);
    $("#segmentDoctor").text(segmentDoctor);
    $("#finalMessage").fadeIn();
}

var titleQuestion1 = items[0];
var question1 = items[1];

var answerSet1 = "";
answerSet1 += "<div class=\"answer answerMiniTxt\" id=\"answer1\" onclick=\"select('1,1')\">";
answerSet1 += "<p>" + items[2] + "</p>";
answerSet1 += "<div class=\"answerSelection answerMini\" id=\"answerSelection1\" ></div>";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer answerMiniTxt\" id=\"answer2\" onclick=\"select('1,2')\">";
answerSet1 += "<p>" + items[3] + "</p>";
answerSet1 += "<div class=\"answerSelection answerMini\" id=\"answerSelection2\" ></div>";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer answerMiniTxt\" id=\"answer3\" onclick=\"select('1,3')\">";
answerSet1 += "<p>" + items[4] + "</p>";
answerSet1 += "<div class=\"answerSelection answerMini\" id=\"answerSelection3\" ></div>";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer answerMiniTxt\" id=\"answer4\" onclick=\"select('1,4')\">";
answerSet1 += "<p>" + items[5] + "</p>";
answerSet1 += "<div class=\"answerSelection answerMini\" id=\"answerSelection4\" ></div>";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer answerMiniTxt\" id=\"answer5\" onclick=\"select('1,5')\">";
answerSet1 += "<p>" + items[6] + "</p>";
answerSet1 += "<div class=\"answerSelection answerMini\" id=\"answerSelection5\" ></div>";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer answerMiniTxt\" id=\"answer6\" onclick=\"select('1,6')\">";
answerSet1 += "<p>" + items[7] + "</p>";
answerSet1 += "<div class=\"answerSelection answerMini\" id=\"answerSelection6\" ></div>";
answerSet1 += "<\/div>";
answerSet1 += "<div class=\"answer answerMiniTxt\" id=\"answer7\" onclick=\"select('1,7')\">";
answerSet1 += "<p>" + items[8] + "</p>";
answerSet1 += "<div class=\"answerSelection answerMini\" id=\"answerSelection7\" ></div>";
answerSet1 += "<\/div>";

var titleQuestion2 = items[9];
var question2 = items[10];

var answerSet2 = "";
answerSet2 += "<div class=\"answer answerMiniTxt\" id=\"answer1\" onclick=\"select('2,1')\">";
answerSet2 += "<p>" + items[11] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection1\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\" id=\"answer2\" onclick=\"select('2,2')\">";
answerSet2 += "<p>" + items[12] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection2\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\" id=\"answer3\" onclick=\"select('2,3')\">";
answerSet2 += "<p>" + items[13] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection3\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\" id=\"answer4\" onclick=\"select('2,4')\">";
answerSet2 += "<p>" + items[14] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection4\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\" id=\"answer5\" onclick=\"select('2,5')\">";
answerSet2 += "<p>" + items[15] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection5\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\" id=\"answer6\" onclick=\"select('2,6')\">";
answerSet2 += "<p>" + items[16] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection6\" ></div>";
answerSet2 += "<\/div>";
answerSet2 += "<div class=\"answer answerMiniTxt\" id=\"answer7\" onclick=\"select('2,7')\">";
answerSet2 += "<p>" + items[17] + "</p>";
answerSet2 += "<div class=\"answerSelection answerMini\" id=\"answerSelection7\" ></div>";
answerSet2 += "<\/div>";

var titleQuestion3 = items[18];
var question3 = items[19];

var answerSet3 = "";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer1\" onclick=\"select('3,1')\">";
answerSet3 += "<p>" + items[20] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection1\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer2\" onclick=\"select('3,2')\">";
answerSet3 += "<p>" + items[21] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection2\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer3\" onclick=\"select('3,3')\">";
answerSet3 += "<p>" + items[22] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection3\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer4\" onclick=\"select('3,4')\">";
answerSet3 += "<p>" + items[23] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection4\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer5\" onclick=\"select('3,5')\">";
answerSet3 += "<p>" + items[24] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection5\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer6\" onclick=\"select('3,6')\">";
answerSet3 += "<p>" + items[25] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection6\" ></div>";
answerSet3 += "<\/div>";
answerSet3 += "<div class=\"answer answerMiniTxt\" id=\"answer7\" onclick=\"select('3,7')\">";
answerSet3 += "<p>" + items[26] + "</p>";
answerSet3 += "<div class=\"answerSelection answerMini\" id=\"answerSelection7\" ></div>";
answerSet3 += "<\/div>";

var titleQuestion4 = items[27];
var question4 = items[28];

var answerSet4 = "";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer1\" onclick=\"select('4,1')\">";
answerSet4 += "<p>" + items[29] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection1\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer2\" onclick=\"select('4,2')\">";
answerSet4 += "<p>" + items[30] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection2\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer3\" onclick=\"select('4,3')\">";
answerSet4 += "<p>" + items[31] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection3\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer4\" onclick=\"select('4,4')\">";
answerSet4 += "<p>" + items[32] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection4\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer5\" onclick=\"select('4,5')\">";
answerSet4 += "<p>" + items[33] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection5\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer6\" onclick=\"select('4,6')\">";
answerSet4 += "<p>" + items[34] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection6\" ></div>";
answerSet4 += "<\/div>";
answerSet4 += "<div class=\"answer answerMiniTxt\" id=\"answer7\" onclick=\"select('4,7')\">";
answerSet4 += "<p>" + items[35] + "</p>";
answerSet4 += "<div class=\"answerSelection answerMini\" id=\"answerSelection7\" ></div>";
answerSet4 += "<\/div>";


var titleQuestion5 = items[36];
var question5 = items[37];

var answerSet5 = "";
answerSet5 += "<div class=\"answer answerMiniTxt\" id=\"answer1\" onclick=\"select('5,1')\">";
answerSet5 += "<p>" + items[38] + "</p>";
answerSet5 += "<div class=\"answerSelection answerMini\" id=\"answerSelection1\" ></div>";
answerSet5 += "<\/div>";
answerSet5 += "<div class=\"answer answerMiniTxt\" id=\"answer2\" onclick=\"select('5,2')\">";
answerSet5 += "<p>" + items[39] + "</p>";
answerSet5 += "<div class=\"answerSelection answerMini\" id=\"answerSelection2\" ></div>";
answerSet5 += "<\/div>";
answerSet5 += "<div class=\"answer answerMiniTxt\" id=\"answer3\" onclick=\"select('5,3')\">";
answerSet5 += "<p>" + items[40] + "</p>";
answerSet5 += "<div class=\"answerSelection answerMini\" id=\"answerSelection3\" ></div>";
answerSet5 += "<\/div>";
answerSet5 += "<div class=\"answer answerMiniTxt\" id=\"answer4\" onclick=\"select('5,4')\">";
answerSet5 += "<p>" + items[41] + "</p>";
answerSet5 += "<div class=\"answerSelection answerMini\" id=\"answerSelection4\" ></div>";
answerSet5 += "<\/div>";
answerSet5 += "<div class=\"answer answerMiniTxt\" id=\"answer5\" onclick=\"select('5,5')\">";
answerSet5 += "<p>" + items[42] + "</p>";
answerSet5 += "<div class=\"answerSelection answerMini\" id=\"answerSelection5\" ></div>";
answerSet5 += "<\/div>";
answerSet5 += "<div class=\"answer answerMiniTxt\" id=\"answer6\" onclick=\"select('5,6')\">";
answerSet5 += "<p>" + items[43] + "</p>";
answerSet5 += "<div class=\"answerSelection answerMini\" id=\"answerSelection6\" ></div>";
answerSet5 += "<\/div>";
answerSet5 += "<div class=\"answer answerMiniTxt\" id=\"answer7\" onclick=\"select('5,7')\">";
answerSet5 += "<p>" + items[44] + "</p>";
answerSet5 += "<div class=\"answerSelection answerMini\" id=\"answerSelection7\" ></div>";
answerSet5 += "<\/div>";

String.prototype.replaceAt = function (index, character) {
    return this.substr(0, index) + character + this.substr(index + character.length);
}